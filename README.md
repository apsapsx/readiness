# Production Readiness


## Overview

Production readiness is necessary whenever there is a significant change
to GitLab.com infrastructure. This may be a new service, a new feature,
or a major infrastructure change.

## Readiness template

The production readiness template is version controlled as an
issue template in the infrastructure issue tracker.

https://gitlab.com/gitlab-com/gl-infra/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md

In some cases this template can be used in an issue, this repository is intended
for larger changes that are better suited for MR reviews.


## Structure

The directory structure of this project has a single directory per
change. In each directory there will be one or more markdown
documents that break the change into different audiences for
reviews. It is not necessary to have more than one markdown
document, it will depend on the size of the change.

### Example

```
YYYY-MM-DD-<feature name>
  \
   - overview.md
      * architecture overview
      * deployment and timeline
      * risk assessment
      * security considerations, secrets
      * database (pg/redis) impacts
   - monitoring.md
      * metrics
      * logging
      * alerting
   - testing.md
      * all testing procedures
```
